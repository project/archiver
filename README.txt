
Description:
------------
This is a simple module providing node archiving capabilities for Drupal.

Features:
---------
* 'Archive this node' checkbox is added to the node edit form
* Archived nodes are chronologically listed at /archiver/
* Optionally, archived nodes can be hidden from the front page (if they were
promoted to the front page) and/or from the categories listings (see INSTALL.txt)

Installation:
-------------

Please see the INSTALL document for details.

Bugs/Features/Patches:
----------------------

If you want to report bugs, feature requests, or submit a patch, please do so
at the project page on the Drupal web site.
http://drupal.org/project/archiver

Author
------
Richard Laffers (rlaffers@gmail.com)
